import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Heuristic class that implements Heuristic interface (strategy pattern)
 * @author SimonLevitt
 * @zid z5084643
 */
public class MyHeuristic implements Heuristic{
	
	HashMap<String, HashMap<String, Integer>> travelCosts;
	HashMap<String, Integer> unloadCosts;
	
	/**
	 * MyHeuristic constructor
	 * @param travelCosts
	 * @param unloadCosts
	 */
	public MyHeuristic(HashMap<String, HashMap<String, Integer>> travelCosts, HashMap<String, Integer> unloadCosts){
		this.travelCosts = travelCosts;
		this.unloadCosts = unloadCosts;
	}
	
	/**
	 * Override method for getHeuristic() in Heuristic interface
	 * h() = Sum of all travel costs between towns of remaining jobs +
	 * unload costs at <dropoff> locations, of <start> state + minimum outgoing edge of <start> if the <start> state is not empty
	 * and there is no job from this location (as completing remaining jobs from here would involve leaving this node)
	 * @param start
	 * @param goal
	 * @return h
	 */
	@Override
	public int getHeuristic(Node start, LinkedList<Job> goal) {
		LinkedList<Job> state = start.getState();
		int h = 0;
		Boolean jobFrom = false;
		String pickup, dropoff, nodeLocation = start.getLocation();
		
		for(Job job : state){
			pickup = job.getFrom();
			dropoff = job.getTo();
			
			// add all edges between jobs, and unload costs (if specified, 0 otherwise), 
			// as a continuous schedule is the shortest possible route.

			h += getTravelCost(pickup, dropoff);
			if(unloadCosts.get(dropoff) != null) h += unloadCosts.get(dropoff);
			
			
			// If there is a job originating from current node in current <state>
			if(pickup.equals(nodeLocation)){
				jobFrom = true;
			}
		}
		// Only add minimum outgoing edge to h() if there is no job from the current node, and there is at least 1 job left in <state>
		// (to avoid adding it twice, as it would have been added in previous step: job <from> <EDGE> <to>). This is added due to
		// the minimum actual distance will have to include one of the edges leaving current node, to finish remaining job/s
		if(! jobFrom && ! state.isEmpty()){
			h += getMinOutgoing(nodeLocation);
		}
		
		return h;
	}
	
	/**
	 * getter method for travel cost between <job> pickup and dropoff points
	 * @param job
	 * @return <cost>
	 */
	public int getTravelCost(String pickup, String dropoff) {
		if(travelCosts.get(pickup).get(dropoff) == null) {
			System.err.println("Invalid Input:\n'" + pickup + "' and '" + dropoff + "' are not directly connected.");
			System.exit(0);
		}
		return travelCosts.get(pickup).get(dropoff);
	}
	
	/**
	 * getter method for minimum outgoing edge value from <start> location
	 * @param start
	 * @return min
	 */
	public int getMinOutgoing(String start){
		
		// All outgoing edges from <start> are sorted in ascending order by cost
		Object[] outgoing = travelCosts.get(start).values().toArray();
		Arrays.sort(outgoing);
		
		// Min outgoing edge cost is in position 0
		return (Integer) outgoing[0];
	}
}