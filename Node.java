import java.util.LinkedList;
import java.util.Objects;

/**
 * Node class containing f(), g(), name <location> and state (list of remaining jobs from current <location>)
 * @author SimonLevitt
 * @zid z5084643
 */
public class Node implements Comparable<Node> {
	private String location;
	private int f;
	private int g;
	private LinkedList<Job> state;
	
	/**
	 * Node constructor
	 * @param location
	 * @param state
	 */
	public Node (String location, LinkedList<Job> state){
		this.state = state;
		this.location = location;
		this.g = this.f = Integer.MAX_VALUE;

	}
	
	/**
	 * setter method for g()
	 * @param g
	 */
	public void setG(int g){
		this.g = g;
	}
	
	/**
	 * setter method for f()
	 * @param f
	 */
	public void setF(int f){
		this.f = f;
	}
	
	/**
	 * getter method for f()
	 * @return f
	 */
	public int getF(){
		return this.f;
	}
	
	/**
	 * getter method for g()
	 * @return g
	 */
	public int getG(){
		return this.g;
	}
	
	/**
	 * getter method for location
	 * @return location
	 */
	public String getLocation(){
		return this.location;
	}
	
	/**
	 * getter method for state
	 * @return state
	 */
	public LinkedList<Job> getState(){
		return this.state;
	}
	
	/**
	 * override method for compareTo(). For use with PriorityQueue in A* search class
	 */
	@Override
	public int compareTo(Node n){
		if(this.getF() == n.getF()) return n.getG() - this.getG();
		return this.getF() - n.getF();
	}
	
	/**
	 * override method for hashCode(). For use with PriorityQueue in A* search class
	 */
    @Override
    public int hashCode() {
        return Objects.hash(g, location, state);
    }
    
	/**
	 * override method for equals(). For use with PriorityQueue in A* search class
	 * that ensures A* ignores longer paths leading to the current node state & location.
	 */
    @Override
    public boolean equals(Object o) {
    	if(o == null) return false;
        if(o == this) return true;
        if(! (o instanceof Node)) return false;
        
        Node node = (Node) o;

        return g == node.g &&
        		location.equals(node.location) &&
        		state.equals(node.state);
    }
}
