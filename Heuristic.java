import java.util.LinkedList;

/**
 * Heuristic interface, to implement strategy pattern
 * @author SimonLevitt
 * @zid z5084643
 */
public interface Heuristic {
	
	// get heuristic value h() from <start> state to <goal> state
	int getHeuristic(Node start, LinkedList<Job> goal);
}
