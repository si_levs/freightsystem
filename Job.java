/**
 * Job class containing pickup and dropoff locations
 * @author SimonLevitt
 * @zid z5084643
 *
 */
public class Job {
	private String from;
	private String to;
	
	/**
	 * Job constructor
	 * @param from
	 * @param to
	 */
	public Job(String from, String to){
		this.from = from;
		this.to = to;
	}
	
	/**
	 * getter method for job pickup location <from>
	 * @return from
	 */
	public String getFrom(){
		return from;
	}
	
	/**
	 * getter method for job dropoff location <to>
	 * @return to
	 */
	public String getTo(){
		return to;
	}
}
