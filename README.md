# README #

Implemented a solution to a problem faced by a small trucking company moving freight around Australia, making optimal use of a single truck. The aim was to schedule an optimal route for the truck to complete a set of jobs, where the truck completes one job at a time. 
While all the jobs on the job list needed to be included in the final route, the journey may have included additional (multiple) road trips from the end of one job to the start of the next job. 
The aim of the system was to schedule an optimal route, or note that there was no solution, if this is the case.

Completed 05/2017 by Simon Levitt