import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * COMP2911 Assignment 2 - Freight Transport System
 * Main class for freight Transport System
 * @author SimonLevitt
 * @zid z5084643
 * Heuristic Analysis:
 * 	For runtime analysis, I have written a perl script to generate input for the program with a varying amounts of nodes. 
 * 	Each variation is built using the same algorithm, therefore producing a similar graph, although larger. I first
 * 	ran the program with my heuristic ('with' in the table below), then the same inputs, yet with no heuristic (h=0 'without'
 * 	in the table below). If the runtime exceeded 15m without terminating, I noted 'TIME' in the table.
 *
 * num. of vertices				  nodes expanded						runtime
 * 								with		without				with			without
 * 		10						   8			 84		 		0.192s			 0.190s
 * 		20						  39		   7409		 		0.169s			 2.020s
 * 		30						 240		   TIME			 	0.289s			 TIME
 * 		40					   20902		   TIME			   48.870s			 TIME
 * 		50					   38422		   TIME		      4m5.338s			 TIME
 * 
 * 	As you can see from the table above, an informed algorithm (with heuristic) is much more efficient in both time
 * 	and space complexities. A graph with just 30 vertices, without a heuristic, was already unusable in any 
 * 	application of importance. This analysis shows the reason for constant study, and need for breakthroughs, in 
 * 	algorithm design.
 * 			
 */

public class FreightSystem {
	public static void main(String[] args) {
	      Scanner sc = null;
	      try
	      {
	          sc = new Scanner(new FileReader(args[0]));
	      }
	      catch (FileNotFoundException e){}
	      finally
	      {
	          if (sc != null){
	        	  String currentLine;
	        	  String[] words;
	        	  HashMap<String, HashMap<String, Integer>> travelCosts = new HashMap<String, HashMap<String, Integer>>();
	        	  HashMap<String, Integer> unloadCosts = new HashMap<String, Integer>();
	        	  HashMap<String, LinkedList<String>> jobMap = new HashMap<String, LinkedList<String>>();
	        	  LinkedList<Job> initialState = new LinkedList<Job>();
	        	 

	        	  while(sc.hasNextLine()){
	        		  
	        		  currentLine = sc.nextLine();
	        		  
	        		  // Ignore all comment only lines
	        		  if(currentLine.matches("\\s*#.*")) continue;
	        		  // Ignore blank lines
	        		  if(currentLine.matches("^\\s*$")) continue;
	        		  // Remove comments from end of line
	        		  currentLine = currentLine.replaceAll("\\s*#.*", "");
	        		  // Split line using spaces as the delimiter, and put into array
	        		  words = currentLine.split(" ");
	        		  
	        		  String currentRequest = words[0];
	        		  
	        		  if(currentRequest.equals("Unloading")){ // Adding an unloading <cost> at <name>
	        			  
	        			  int cost = Integer.parseInt(words[1]);
	        			  String name = words[2];
	        			  unloadCosts.put(name, cost);

	        		  } else if(currentRequest.equals("Cost")){ // Adding undirected <cost> to travel from <from> to <to>
	        			  
	        			  int cost = Integer.parseInt(words[1]);
	        			  String from = words[2];
	        			  String to = words[3];
	        			  
	        			  if(travelCosts.containsKey(from)){
	        				  travelCosts.get(from).put(to, cost);
	        			  } else {
	        				  HashMap<String, Integer> hash1 = new HashMap<String, Integer>();
	        				  hash1.put(to, cost);
	        				  travelCosts.put(from, hash1);
	        			  }
	        			  
	        			  if(travelCosts.containsKey(to)){ // Add cost in other direction
	        				  travelCosts.get(to).put(from, cost);
	        			  } else {
	        				  HashMap<String, Integer> hash2 = new HashMap<String, Integer>();
	        				  hash2.put(from, cost);
	        				  travelCosts.put(to, hash2);
	        			  }
	        			  
	        			  
	        		  } else if(currentRequest.equals("Job")){ // Job is required from <pickup> to <dropoff>
	        			  
	        			  String pickup = words[1];
	        			  String dropoff = words[2];
	        			  Job job = new Job(pickup, dropoff);
	        			  initialState.add(job); 
	        			  
	        			  if(! jobMap.containsKey(pickup)){
	        				  LinkedList<String> list = new LinkedList<String>();
	        				  list.add(dropoff);
	        				  jobMap.put(pickup, list);
	        			  } else {
	        				  jobMap.get(pickup).add(dropoff);
	        			  }
	        			  
	        		  }	  
	        	  }
	        	  
	        	  // Astar(Heuristic, initialState, TownLinks, travelCosts, unloadCosts)
	        	  Astar search = new Astar(new MyHeuristic(travelCosts, unloadCosts), initialState, jobMap, travelCosts, unloadCosts);
	        	  System.out.println(search.searchResult());
	        	  
	        	  sc.close();
	          }
	          
	      }
	}
}
