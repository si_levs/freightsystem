import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * A* search class
 * @author SimonLevitt
 * @zid z5084643
 */
public class Astar {
	private LinkedList<Job> initialState;
	private LinkedList<Job> goal;
	private PriorityQueue<Node> open;
	private PriorityQueue<Node> closed;
	private Heuristic h1;
	private HashMap<String, HashMap<String, Integer>> travelCosts;
	private HashMap<String, Integer> unloadCosts;
	private HashMap<String, LinkedList<String>> jobMap;
	
	/**
	 * Astar constructor
	 * @param h1
	 * @param initialState
	 * @param jobMap
	 * @param travelCosts
	 * @param unloadCosts
	 */
	public Astar(Heuristic h1, LinkedList<Job> initialState, HashMap<String, LinkedList<String>> jobMap, HashMap<String, HashMap<String, Integer>> travelCosts, HashMap<String, Integer> unloadCosts){
		this.travelCosts = travelCosts;
		this.unloadCosts = unloadCosts;
		this.initialState = initialState;
		this.h1 = h1;
		this.jobMap = jobMap;
		this.goal = new LinkedList<Job>(); // goal state is an empty job list
	}
	
	public String searchResult(){

		closed = new PriorityQueue<Node>();
		open = new PriorityQueue<Node>();
		
		Node current;
		Node start = new Node("Sydney", initialState);
		int nodesExpanded = 0;
		
		start.setG(0);
		
		start.setF(h1.getHeuristic(start, goal));

		open.add(start);
		
		HashMap<Node, Node> cameFrom = new HashMap<Node, Node>();
		HashMap<Node, Integer> gScore = new HashMap<Node, Integer>();
		
		gScore.put(start, 0);
		
		while(! open.isEmpty()){
			current = open.poll();
			nodesExpanded++;

			if(current.getState().equals(goal)){
				return reconstructPath(cameFrom, current, nodesExpanded);
			}
			
			closed.add(current);
			
			LinkedList<Node> neighbours = getNeighbours(current);
			
			for(Node neighbour : neighbours){

				if(closed.contains(neighbour)) continue; // Ignore previously expanded nodes
				
				if(gScore.get(neighbour) == null) gScore.put(neighbour, Integer.MAX_VALUE); // Min g() recorded thus far of each node is set to MAX by default
				
				if(! open.contains(neighbour)){ // New node to explore
					open.add(neighbour);
				} else if (neighbour.getG() >= gScore.get(neighbour)){ // This path is equal to, or worse than one previously explored so ignore it
					continue;
				}

				cameFrom.put(neighbour, current); // Save most efficient route to <neighbour> thus far
				gScore.put(neighbour, neighbour.getG()); // Smallest actual cost path to reach <neighbour> found thus far
				
			}
		}
		// return if no path found
		return nodesExpanded + " nodes expanded\nNo Solution";
	}
	
	/**
	 * Method to reconstruct the optimal path found from initial state to goal state
	 * @param cameFrom
	 * @param current
	 * @param nodesExpanded
	 * @return path
	 */
	public String reconstructPath(HashMap<Node, Node> cameFrom, Node current, int nodesExpanded){
		LinkedList<Node> totalPath = new LinkedList<Node>();
		totalPath.add(current);
		while(cameFrom.keySet().contains(current)){
			current = cameFrom.get(current);
			if(current != null) totalPath.addFirst(current);
		}
		return toString(totalPath, nodesExpanded);
	}
	
	/**
	 * Method to return linked list of the neighbours of <current>
	 * @param current
	 * @return neighbours
	 */
	public LinkedList<Node> getNeighbours(Node current){
		LinkedList<Node> neighbours = new LinkedList<Node>();
		Set<String> children = travelCosts.get(current.getLocation()).keySet();
		
		for(String child : children){
			Node adj = new Node(child, getState(current, child));
			adj.setG(getG(current, adj));
			adj.setF(adj.getG() + h1.getHeuristic(adj, goal));
			neighbours.add(adj);
		}
		
		return neighbours;
	}
	
	public int getG(Node current, Node neighbour){
		int g = current.getG() + getCost(current, neighbour);
		
		// If current <state> and <neighbour> state are not equal, there was a job between the two locations, 
		// so the cost to get to this neighbour must include the dropoff cost (if specified, 0 otherwise).
		if(! current.getState().equals(neighbour.getState())){
			if(unloadCosts.get(neighbour.getLocation()) != null) g += unloadCosts.get(neighbour.getLocation());
		}
		return g;
	}
	
	/**
	 * Method to return updated state after traveling between <current.from> to <neighbour>
	 * @param current
	 * @param neighbour
	 * @return state
	 */
	public LinkedList<Job> getState(Node current, String neighbour){
		LinkedList<Job> state = new LinkedList<Job>(current.getState());
		
		for(Job job : state){
			if(job.getFrom().equals(current.getLocation()) && job.getTo().equals(neighbour)){
				state.remove(job);
				break;
			}
		}

		return state;
	}
	
	/**
	 * Method to get cost of travel between <current> and <neighbour>
	 * @param current
	 * @param neighbour
	 * @return cost
	 */
	public int getCost(Node current, Node neighbour){
		String from = current.getLocation();
		String to = neighbour.getLocation();
		return travelCosts.get(from).get(to);
	}
	
	/**
	 * toString method for the search results in desired format
	 * @param path
	 * @param nodesExpanded
	 * @return string
	 */
	public String toString(LinkedList<Node> path, int nodesExpanded){
		String string = nodesExpanded + " nodes expanded\n";
		
		// If there was a job required between 2 disconnected towns, path will == null
		if(path == null) {
			string += "No Solution";
			return string;
		}
		ListIterator<Node> it = path.listIterator();
		
		
		int cost = path.getLast().getG();
		string += "cost = " + cost + "\n";
		Node pickup = it.next();
		Node destination;
		while(it.hasNext()){
			destination = it.next();
			Boolean jobBetween = false;
			
			// If there is at least 1 job from i-1 path node <location>
			if(jobMap.containsKey(pickup.getLocation())){
				// For every job from current path node <location>
				for(String dropoff : jobMap.get(pickup.getLocation())){
					// Note if there is a job between previous path node (i-1) <location>, and current path node (i) <location>
					if(dropoff.equals(destination.getLocation())){
						jobBetween = true;
					}
				}
			}
			if(jobBetween){
				string += "Job ";
			} else {
				string += "Empty ";
			}
			string += pickup.getLocation() + " to " + destination.getLocation() + "\n";
			pickup = destination;
		}
		string = string.trim();
		return string;
	}
}